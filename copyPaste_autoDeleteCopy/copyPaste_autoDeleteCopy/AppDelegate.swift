//
//  AppDelegate.swift
//  copyPaste_autoDeleteCopy
//
//  Created by Admin on 3/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        print("a")
//        ViewController().detectCopyToClipboard()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("b")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("c")
        ViewController().registerBackgroundTask()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("d")
        ViewController().endBackgroundTask()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        ViewController().notRunningTask()
    }


}

