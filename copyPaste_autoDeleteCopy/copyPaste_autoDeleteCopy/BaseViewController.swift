//
//  BaseViewController.swift
//  copyPaste_autoDeleteCopy
//
//  Created by Admin on 3/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var time = 0
    var timer = Timer()
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    var stopCopy = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func detectCopyToClipboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(handlerCopyToClipboard), name: UIPasteboard.changedNotification, object: nil)
    }
    
    @objc func handlerCopyToClipboard(){
        print("copy")
        time = 20
        timer.invalidate()
        self.countDown(with: time)
    }
    
    //thoát ra background thì gọi
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
    }
    
    //vào lại app thì gọi
    func endBackgroundTask() {
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
    }
    func notRunningTask(){
        UIPasteboard.general.string = ""
    }
    func countDown(with number: Int){
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(runningTime), userInfo: nil, repeats: true)
    }
    
    @objc func runningTime(){
        print(time)
        time -= 1
        if time == 0 {
            UIPasteboard.general.string = ""
            timer.invalidate()
        }
    }
}
