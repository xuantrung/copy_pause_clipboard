//
//  ViewController.swift
//  copyPaste_autoDeleteCopy
//
//  Created by Admin on 3/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var pasteTextField: UITextField!
    @IBOutlet weak var copyTextField: UITextField!
   
    @IBOutlet weak var hiddenMenuButton: UIButton!
    @IBOutlet weak var sideMenu: UIView!
    @IBOutlet weak var MenuTableView: UITableView!
    var menuIsHidden = true
    var opacity = 0
    
    var icon_tableView = ["icon_help_privacy-policy", "icon_setting_change_pki-key", "icon_setting_edit-account", "icon_setting_language", "iconSettingDeviceManage", "menu-icon_settings", "menuIconSubscription"]
    var label_tableView = ["Manage Security", "Change Key", "Edit Account", "Language", "Manage Device", "Setting", "Subscribe"]

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureNavigationBar()
        configSwipeSideMenu()
        self.copyTextField.delegate = self
        self.detectCopyToClipboard()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        configureSideMenu()
    }
    
    func configureSideMenu(){
        hiddenMenuButton.isHidden = true
        sideMenu.backgroundColor = .darkGray
        MenuTableView.backgroundColor = .darkGray
        self.sideMenu.center.x = -self.sideMenu.frame.size.width
    }
    @IBAction func clickHiddenMenuBtn(_ sender: Any) {
        handleMenuToggle()
    }
    
    func transferSlideMenu(){
        
        if menuIsHidden{
            UIView.animate(withDuration: 0.7) {
                self.sideMenu.center.x = -self.sideMenu.frame.size.width
                self.opacity = 0
                self.hiddenMenuButton.backgroundColor = UIColor.black.withAlphaComponent(CGFloat(self.opacity)/10)
                self.navigationController?.navigationBar.barTintColor = UIColor.darkGray.withAlphaComponent(CGFloat(self.opacity)/10)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.7) {
                self.hiddenMenuButton.isHidden = true
            }
        }
        else{
            hiddenMenuButton.isHidden = false
            UIView.animate(withDuration: 0.7) {
                self.sideMenu.center.x = self.sideMenu.frame.size.width/2
                self.opacity = 10
                self.hiddenMenuButton.backgroundColor = UIColor.black.withAlphaComponent(CGFloat(self.opacity)/10)
                self.navigationController?.navigationBar.barTintColor = UIColor.darkGray.withAlphaComponent(CGFloat(self.opacity)/10)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.navigationController?.navigationBar.isHidden = !(self.navigationController?.navigationBar.isHidden)!
        }
    }
    func configSwipeSideMenu(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeMenu(sender:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeMenu(sender:)))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
    }

    @objc func swipeMenu(sender: UISwipeGestureRecognizer){
        let local = sender.location(in: self.view)
        switch sender.direction {
        case .right:
            //swipe from left vertical
            if local.x > 5{
                return
            }
        case .left:
            //swipe only when side menu displayed
            if menuIsHidden{
                return
            }
        default:
            return
        }
        handleMenuToggle()
    }
    
    func configureNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuToggle))
        navigationItem.title = "Slide menu"
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        navigationController?.navigationBar.barStyle = .black
        
//        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 45))
//        navBar.barTintColor = .darkGray
//        navBar.barStyle = .black
//        navBar.setItems([navigationItem], animated: false)
       // self.view.addSubview(navBar)
    }
    @objc func handleMenuToggle(){
        menuIsHidden = !menuIsHidden
        transferSlideMenu()
    }
    
    @IBAction func transfer(_ sender: Any) {
        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "VC2") as! TestViewController
        self.present(vc2, animated: true)
    }
    
    @IBAction func clickButton(_ sender: Any) {
        UIPasteboard.general.string = self.copyTextField.text ?? ""
        handlerCopyToClipboard()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("begin")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        copyTextField.resignFirstResponder() //hidden keyboard
        print("return")
        return true
    }
}

